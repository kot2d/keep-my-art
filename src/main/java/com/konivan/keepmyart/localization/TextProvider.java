package com.konivan.keepmyart.localization;

import com.konivan.keepmyart.views.localization.TextSource;

public abstract class TextProvider {

    public abstract String getErrorText(TextSource textSource);
    public abstract String getText(TextSource textSource);

}

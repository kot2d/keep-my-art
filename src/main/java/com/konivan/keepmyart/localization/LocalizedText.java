package com.konivan.keepmyart.localization;

import com.konivan.keepmyart.model.Localization;
import com.konivan.keepmyart.services.KeepService;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.List;
import java.util.Locale;


public class LocalizedText {
    static ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();


    KeepService keepService;

    public LocalizedText(KeepService keepService) {
        this.keepService = keepService;
    }

    public String getText(String textPage, String textId) {
        List<Localization> localizations = keepService.getActiveLocalization();
        Locale locale;
        if( localizations.isEmpty()) {
            locale = new Locale("ru");
        } else {
            locale = new Locale(localizations.get(0).getLang(), localizations.get(0).getCountry());
        }
        messageSource.setBasenames(textPage);
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource.getMessage(textId, null, locale);
    }
}

package com.konivan.keepmyart.services.controllers;

import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.ModelConstants;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.model.WebUrls;
import com.konivan.keepmyart.services.clouds.YandexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@Slf4j
@Controller
@RequestMapping("/" + WebUrls.OAUTH2)
public class OauthController {

    YandexService yaService;

    OauthController() {
        this.yaService = new YandexService();
    }

    @GetMapping
    public String home(@RequestParam String source, Model model) {
        if(SourceTypes.valueOf(source).equals(SourceTypes.YANDEX)){
            model.addAttribute("oauthLink", yaService.getAuthorizationUrl());
            model.addAttribute("buttonName", ModelConstants.YANDEX_DISK_NAME);
        }
        return "oauth_base";
    }
    @GetMapping("/yandex_old")
    public String oauth(@RequestParam String code, final RedirectAttributes redirectAttributes) throws IOException, ExecutionException, InterruptedException {
        yaService.doOauth(code);

        KeepSource source = new KeepSource();
        source.setCloudToken(yaService.getOauthToken());
        source.setSourceType(SourceTypes.YANDEX);
        source.setUserName(yaService.getUserInfo(yaService.getOauthToken()).getLogin());

        redirectAttributes.addFlashAttribute("keepSource", source);

        return "redirect:/"+ WebUrls.NEW_PROJECT + "/" + WebUrls.ADD_CLOUD;
    }
}

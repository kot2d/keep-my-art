package com.konivan.keepmyart.services.controllers;

import com.konivan.keepmyart.model.*;
import com.konivan.keepmyart.services.KeepService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Controller
@RequestMapping("/" + WebUrls.PROJECT_DATA)
@SessionAttributes("sourceList")
public class ProjectDataController {

    @Autowired
    KeepService service;

    @GetMapping()
    public String removeCloud(@RequestParam String id, Model model) {
        AtomicReference<String> templateSource = new AtomicReference<>("projectdata");
        Optional<KeepProject> project = service.getProject(Long.parseLong(id));
        project.ifPresentOrElse(keepProject-> {
            List<KeepSource> keepSources = service.getSourcesByProject(keepProject);
            model.addAttribute("projectName", keepProject.getName());
            model.addAttribute("localSourceList", keepSources.stream().filter(source -> source.getSourceType().equals(SourceTypes.LOCAL)).toList());
            model.addAttribute("cloudSourceList", keepSources.stream().filter(source -> source.getSourceType().equals(SourceTypes.YANDEX)).toList());
        }, () -> {templateSource.set("redirect:/");});
        return templateSource.get();
    }
}

package com.konivan.keepmyart.services.controllers;

import com.konivan.keepmyart.model.*;
import com.konivan.keepmyart.services.KeepService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

@Slf4j
@Controller
@RequestMapping("/" + WebUrls.NEW_PROJECT + "old")
@SessionAttributes("sourceList")
public class NewProjectController {

    @Autowired
    KeepService service;
    @ModelAttribute(name = "newKeepProject")
    public KeepProject newProject() {
        return new KeepProject();
    }

    @ModelAttribute(name = "sourceList")
    public KeepSources projectSources() {
        return new KeepSources();
    }


    @PostMapping
    public String saveProject(@Valid @ModelAttribute KeepProject newKeepProject, Errors errors,
                              @ModelAttribute("sourceList") KeepSources keepSources,
                              Model model, SessionStatus sessionStatus) {
        if (errors.hasErrors()) {
            return buildHome(keepSources, model);
        }

        service.saveProject(newKeepProject);
        keepSources.forEach(source -> {
            source.setKeepProject(newKeepProject);
            if(source.getSourceType().equals(SourceTypes.YANDEX) || source.getSourceType().equals(SourceTypes.GOOGLE)) {
                source.setSourcePath(newKeepProject.getName());
            }
        });
        service.saveSources(keepSources.stream().toList());
        sessionStatus.setComplete();
        return "redirect:";
    }

    @GetMapping("/" + WebUrls.ADD_LOCAL_PATH)
    public String addLocalPath(@RequestParam String localPath, @ModelAttribute("sourceList") KeepSources keepSources, Model model) {
        if(localPath.isBlank()){
            model.addAttribute("errorLocalPath", "Локальный путь не может быть пустым!");
        } else {
            KeepSource localSource = new KeepSource();
            localSource.setSourcePath(localPath);
            localSource.setSourceType(SourceTypes.LOCAL);
            keepSources.add(localSource);
            model.addAttribute("sourceList", keepSources);
        }
        return "redirect:/" + WebUrls.NEW_PROJECT;
    }

    @GetMapping("/" + WebUrls.REMOVE_LOCAL_PATH)
    public String removeLocalPath(@RequestParam String localPath, @ModelAttribute("sourceList") KeepSources keepSources, Model model) {
        keepSources.removeIf(source -> source.getSourceType().equals(SourceTypes.LOCAL) && source.getSourcePath().equals(localPath));
        model.addAttribute("sourceList", keepSources);
        return "redirect:/" + WebUrls.NEW_PROJECT;
    }

    @GetMapping("/" + WebUrls.ADD_CLOUD)
    public String addCloud(@ModelAttribute("sourceList") KeepSources keepSources,
                           @ModelAttribute KeepSource keepSource,
                           Model model) {
        keepSources.add(keepSource);
        model.addAttribute("sourceList", keepSources);
        return "redirect:/" + WebUrls.NEW_PROJECT;
    }

    @GetMapping("/" + WebUrls.REMOVE_CLOUD)
    public String removeCloud(@RequestParam String userName, @RequestParam String cloudType, @ModelAttribute("sourceList") KeepSources keepSources,
                           @ModelAttribute KeepSource keepSource,
                           Model model) {
        keepSources.removeIf(source -> source.getSourceType().equals(SourceTypes.valueOf(cloudType)) && source.getUserName().equals(userName));
        model.addAttribute("sourceList", keepSources);
        return "redirect:/" + WebUrls.NEW_PROJECT;
    }

    @GetMapping
    public String start(@ModelAttribute("sourceList") KeepSources keepSources, Model model) {
        return buildHome(keepSources, model);
    }

    private String buildHome(KeepSources keepSources, Model model) {
        model.addAttribute("yandexSource", SourceTypes.YANDEX);
        model.addAttribute("localSourceList", keepSources.stream().filter(source -> source.getSourceType().equals(SourceTypes.LOCAL)).toList());
        model.addAttribute("cloudSourceList", keepSources.stream().filter(source -> source.getSourceType().equals(SourceTypes.YANDEX)).toList());

        return "newproject";
    }

}

package com.konivan.keepmyart.services.controllers;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.WebUrls;
import com.konivan.keepmyart.services.KeepService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Slf4j
@Controller
@RequestMapping()
public class HomeController {

    @Autowired
    KeepService keepService;
    @GetMapping()
    public String init(Model model){
        List<KeepProject> projectList = keepService.getAllProjects();
        model.addAttribute("projectList", projectList);
        return "index";
    }
}

package com.konivan.keepmyart.services.persistence;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.Localization;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LocalizationRepository extends CrudRepository<Localization, Long> {
    public List<Localization> findLocalizationByActiveTrue();
}

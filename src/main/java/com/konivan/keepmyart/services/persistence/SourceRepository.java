package com.konivan.keepmyart.services.persistence;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SourceRepository extends CrudRepository<KeepSource, Long> {
    List<KeepSource> findByKeepProject(KeepProject project);
}

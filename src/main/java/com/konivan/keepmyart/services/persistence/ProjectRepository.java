package com.konivan.keepmyart.services.persistence;

import com.konivan.keepmyart.model.KeepProject;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<KeepProject, Long> {

}

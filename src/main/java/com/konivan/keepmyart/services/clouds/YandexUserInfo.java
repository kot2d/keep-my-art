package com.konivan.keepmyart.services.clouds;

import lombok.Data;

@Data
public class YandexUserInfo {
    private String login;
    private String id;
    private String client_id;
    private String psuid;
}

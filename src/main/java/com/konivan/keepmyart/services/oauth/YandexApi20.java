package com.konivan.keepmyart.services.oauth;

import com.github.scribejava.apis.openid.OpenIdJsonTokenExtractor;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;

public class YandexApi20 extends DefaultApi20{

        protected YandexApi20() {
        }

        private static class InstanceHolder {
            private static final YandexApi20 INSTANCE = new YandexApi20();
        }

        public static YandexApi20 instance() {
            return InstanceHolder.INSTANCE;
        }

        @Override
        public String getAccessTokenEndpoint() {
            return "https://oauth.yandex.ru/token";
        }

        @Override
        protected String getAuthorizationBaseUrl() {
            return "https://oauth.yandex.com/authorize";
        }

        @Override
        public TokenExtractor<OAuth2AccessToken> getAccessTokenExtractor() {
            return OpenIdJsonTokenExtractor.instance();
        }

        @Override
        public String getRevokeTokenEndpoint() {
            return "https://oauth.yandex.com/revoke_token";
        }
}

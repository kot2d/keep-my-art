package com.konivan.keepmyart.services.configuration;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@SpringComponent
@VaadinSessionScope
@Data
public class NewProjectSessionService {

    private KeepProject project = new KeepProject();

    public void close() {
        project = new KeepProject();
    }
}

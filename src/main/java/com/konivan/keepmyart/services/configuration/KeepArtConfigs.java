package com.konivan.keepmyart.services.configuration;

import com.konivan.keepmyart.model.WebUrls;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeepArtConfigs {

    @Bean
    public WebUrls webUrls() {
        return new WebUrls();
    }
}

package com.konivan.keepmyart.services;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.Localization;
import com.konivan.keepmyart.services.persistence.LocalizationRepository;
import com.konivan.keepmyart.services.persistence.ProjectRepository;
import com.konivan.keepmyart.services.persistence.SourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class KeepService {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    SourceRepository sourceRepository;

    @Autowired
    LocalizationRepository localizationRepository;

    public List<Localization> getActiveLocalization () {
        return localizationRepository.findLocalizationByActiveTrue();
    }

    public KeepProject saveProject(KeepProject project){
            return projectRepository.save(project);
    }

    public Optional<KeepProject> getProject(long id){
        return projectRepository.findById(id);
    }

    public List<KeepSource> getSourcesByProject(KeepProject project){
        return sourceRepository.findByKeepProject(project);
    }

    public void saveSource(KeepSource source){
            sourceRepository.save(source);
    }

    public void saveSources(List<KeepSource> sources){
        sourceRepository.saveAll(sources);
    }

    public List<KeepProject> getAllProjects(){
            List<KeepProject> actualList = StreamSupport
                    .stream(projectRepository.findAll().spliterator(), false)
                    .collect(Collectors.toList());
            return actualList;
    }
}

package com.konivan.keepmyart.services;

import com.konivan.keepmyart.localization.LocalizedText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeepAppConfiguration {

    @Autowired
    KeepService keepService;
    @Bean
    public LocalizedText localizedText() {
        return new LocalizedText(keepService);
    }

}

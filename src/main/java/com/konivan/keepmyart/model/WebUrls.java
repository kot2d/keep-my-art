package com.konivan.keepmyart.model;

public class WebUrls {
    final static public String NEW_PROJECT = "new_project";
    final static public String PROJECT_DATA = "project_data";
    final static public String SAVE = "save";
    final static public String ADD_LOCAL_PATH = "add_local_path";
    final static public String REMOVE_LOCAL_PATH = "remove_local_path";
    final static public String ADD_CLOUD = "add_cloud";
    final static public String REMOVE_CLOUD = "remove_cloud";
    final static public String OAUTH2 = "oauth2";
    final static public String OAUTH2_YANDEX = "oauth2_yandex";
}

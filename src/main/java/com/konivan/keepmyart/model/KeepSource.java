package com.konivan.keepmyart.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NonNull;

@Data
@Entity
public class KeepSource {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne()
    KeepProject keepProject;

    @NotBlank(message = "Local Path could be empty")
    private String sourcePath;

    private String userName;

    private String cloudToken;

    @NotNull
    private SourceTypes sourceType;
}

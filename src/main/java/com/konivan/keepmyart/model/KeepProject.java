package com.konivan.keepmyart.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class KeepProject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Size(min=3, message="Имя проекта должно быть хотя бы 3 символа!")
    private String name;

    @Transient
    private final List<KeepSource> sourceList;
    public KeepProject(List<KeepSource> sourceList) {
        this.sourceList = sourceList;
        this.sourceList.forEach(keepSource -> keepSource.setKeepProject(this));
    }

    public KeepProject() {
        this(new ArrayList<>());
    }

    public void addSource(@NotNull KeepSource item) {
        item.setKeepProject(this);
        sourceList.add(item);
    }

    public void addSources(@NotNull @NotEmpty List<KeepSource> items) {
        items.forEach(keepSource -> keepSource.setKeepProject(this));
        sourceList.addAll(items);
    }

    public void removeSource(@NotNull KeepSource item) {
        sourceList.remove(item);
    }

    public List<KeepSource> getSources() {
        return sourceList;
    }
}

package com.konivan.keepmyart;

import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@NpmPackage(value = "@fontsource/arimo", version = "4.5.0")
@Theme(value = "keepart", variant = Lumo.DARK)
public class KeepMyArtApplication implements AppShellConfigurator {

	public static void main(String[] args) {
		SpringApplication.run(KeepMyArtApplication.class, args);
	}

}

package com.konivan.keepmyart.views;

public class ViewStatic {
    final public static String ROOT_PREFIX = "❯";
    final public static String SUB_PREFIX = "❯❯";
    final public static String ELEMENT_PREFIX = "―";
    final public static String ELEMENT_DELETE = "✖";
    final public static String ELEMENT_ADD = "╋";
}

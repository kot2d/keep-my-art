package com.konivan.keepmyart.views.home;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.model.WebUrls;
import com.konivan.keepmyart.views.MainLayout;
import com.konivan.keepmyart.views.components.CreateProjectButton;
import com.konivan.keepmyart.views.components.ViewRootComponent;
import com.konivan.keepmyart.views.localization.CommonTextProvider;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("Home")
@Route(value = "home", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class HomeView extends ViewRootComponent {

    public HomeView(@Autowired LocalizedText localizedText) {
        super("Home");
        CommonTextProvider commonTextProvider = new CommonTextProvider(localizedText);
        this.addComponent(new CreateProjectButton(commonTextProvider));
    }

}

package com.konivan.keepmyart.views.components.icons;

import com.vaadin.flow.component.icon.VaadinIcon;

public class LocalSourceIconComponent extends IconComponent{
    public LocalSourceIconComponent() {
        super(VaadinIcon.HARDDRIVE);
    }
}

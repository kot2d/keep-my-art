package com.konivan.keepmyart.views.components;

import com.konivan.keepmyart.localization.TextProvider;
import com.konivan.keepmyart.model.WebUrls;
import com.konivan.keepmyart.views.localization.CommonTextProvider;
import com.vaadin.flow.component.button.Button;

public class CreateProjectButton extends Button {

    public CreateProjectButton(TextProvider textProvider) {
        setText(textProvider.getText(CommonTextProvider.BUTTON_CREATE_PROJECT));
        this.addClickListener(e -> this.getUI().ifPresent(ui -> ui.navigate(WebUrls.NEW_PROJECT)));
    }
}

package com.konivan.keepmyart.views.components;

import com.vaadin.flow.component.notification.NotificationVariant;

public class ErrorNotificationComponent extends BaseNotification{

    public ErrorNotificationComponent(String errorMessage){
        super(errorMessage);
        getNotification().addThemeVariants(NotificationVariant.LUMO_ERROR);
        getNotification().setDuration(5000);
    }
}

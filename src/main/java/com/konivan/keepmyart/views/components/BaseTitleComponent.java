package com.konivan.keepmyart.views.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class BaseTitleComponent extends HorizontalLayout {

    private Component prefix;
    private String title;

    public BaseTitleComponent(String prefix, String title) {
        this(new Span(prefix), title);
    }

    public BaseTitleComponent(Component prefix, String title) {
        this.prefix = prefix;
        this.title = title;
        this.setSpacing(true);
        this.setWidthFull();
        this.add(this.prefix, new Span(this.title));
    }

    public Component getPrefix() {
        return prefix;
    }

    public String getTitle() {
        return title;
    }
}

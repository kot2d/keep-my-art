package com.konivan.keepmyart.views.components.list;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.List;
import java.util.function.Function;

public class BaseListComponent<T> extends VerticalLayout {

    private final Function<T,Component> render;
    public BaseListComponent(List<T> items, Function<T,Component> render) {
        this.setJustifyContentMode(JustifyContentMode.CENTER);
        this.render = render;
        items.forEach(item -> {
            this.add(render.apply(item));
        });
    }

    public void addItem(T item){
        this.add(render.apply(item));
    }

}

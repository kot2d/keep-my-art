package com.konivan.keepmyart.views.components.list;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;

import java.util.Objects;

public class ListPrefixComponent {

    private String prefixString;
    public ListPrefixComponent(String prefixString){
        this.prefixString = prefixString;
    }

    public Component initComponent(){
        if(Objects.nonNull(prefixString)) return new Span(prefixString);
        return new Span(prefixString);
    }
}

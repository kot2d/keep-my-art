package com.konivan.keepmyart.views.components.list;

import com.konivan.keepmyart.views.ViewStatic;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class ReadOnlyListComponent<T> extends BaseListComponent<T> {
    public ReadOnlyListComponent(List<T> items, Function<T, Component> render, String cssClassName) {
        super(items, render);
        if(!Objects.isNull(cssClassName)) this.addClassName("list-root-container");
    }



    public static class Builder<R> {
        private Function<R, String> titleGenerator;
        private List<R> items;
        private Function<R, Component> render;
        private String cssClassName = "list-root-container";
        ListPrefixComponent prefixComponent = new ListPrefixComponent(ViewStatic.ELEMENT_PREFIX);

        public Builder<R> setTitleGenerator(Function<R, String> titleGenerator) {
            this.titleGenerator = titleGenerator;
            return this;
        }
        public Builder<R> setRender(Function<R, Component> render) {
            this.render = render;
            return this;
        }
        public Builder<R> setItems(List<R> items) {
            this.items = items;
            return this;
        }

        public Builder<R> setDefaultCssClassName(){
            this.cssClassName = null;
            return this;
        }

        public ReadOnlyListComponent<R> build(){
            titleGenerator = Objects.isNull(titleGenerator) ? Object::toString : titleGenerator;
            render = Objects.isNull(render) ? item -> RemovableElement.init(item, titleGenerator, prefixComponent) : render;
            return new ReadOnlyListComponent<R>(items, render, cssClassName);
        }
    }
    public static class RemovableElement {

        public static <T> HorizontalLayout init(T sourceElement, Function<T, String> titleGenerator, ListPrefixComponent prefixComponent) {
            HorizontalLayout root = new HorizontalLayout();
            root.setWidthFull();
            root.setDefaultVerticalComponentAlignment(Alignment.CENTER);
            root.setClassName("list-element");
            HorizontalLayout titleLayout = new ElementListComponent(prefixComponent.initComponent(), titleGenerator.apply(sourceElement));
            root.add(titleLayout);
            return root;
        }
    }
}

package com.konivan.keepmyart.views.components.icons;

import com.vaadin.flow.component.icon.VaadinIcon;

public class CloudIconComponent extends IconComponent{
    public CloudIconComponent() {
        super(VaadinIcon.CLOUD);
    }
}

package com.konivan.keepmyart.views.components.list;

public class CloudTitleGenerator {

    public final static String YANDEX_PREFIX = "Yandex@";
    public final static String GOOGLE_PREFIX = "Google@";

    public static String getYandexTitle(String title) {
        return YANDEX_PREFIX + title;
    }

    public static String getGoogleTitle(String title) {
        return GOOGLE_PREFIX + title;
    }

}

package com.konivan.keepmyart.views.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class ViewRootComponent extends VerticalLayout {

    VerticalLayout innerLayout = new VerticalLayout();
    public ViewRootComponent(String title){
        this.setAlignItems(FlexComponent.Alignment.CENTER);
        this.innerLayout.setWidth("50%");
        this.addClassName("root-container");
        this.innerLayout.add(new TitleComponent(title));
        add(this.innerLayout);
    }

    public void addComponent(Component ... components){
        this.innerLayout.add(components);
    }

}

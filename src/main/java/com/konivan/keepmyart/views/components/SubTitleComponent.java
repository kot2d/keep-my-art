package com.konivan.keepmyart.views.components;

import com.konivan.keepmyart.views.ViewStatic;

public class SubTitleComponent extends BaseTitleComponent {
    public SubTitleComponent(String title) {
        super(ViewStatic.SUB_PREFIX, title);
    }
}

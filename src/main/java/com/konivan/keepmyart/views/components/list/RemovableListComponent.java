package com.konivan.keepmyart.views.components.list;

import com.konivan.keepmyart.views.ViewStatic;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class RemovableListComponent<T> extends BaseListComponent<T> {
    public RemovableListComponent(List<T> items, Function<T, Component> render) {
        super(items, render);
        this.addClassName("list-root-container");
    }

    public static class Builder<R> {
        private Function<R, String> titleGenerator;
        private List<R> items;
        private Consumer<R> removeAction;
        private Function<R, Component> render;


        public Builder<R> setTitleGenerator(Function<R, String> titleGenerator) {
            this.titleGenerator = titleGenerator;
            return this;
        }

        public Builder<R> setRemoveAction(Consumer<R> removeAction) {
            this.removeAction = removeAction;
            return this;
        }

        public Builder<R> setItems(List<R> items) {
            this.items = items;
            return this;
        }

        public Builder<R> setRender(Function<R, Component> render) {
            this.render = render;
            return this;
        }

        public RemovableListComponent<R> build(){
            removeAction = Objects.isNull(removeAction) ? item -> items.remove(item) : removeAction;
            titleGenerator = Objects.isNull(titleGenerator) ? Object::toString : titleGenerator;
            render = Objects.isNull(render) ? item -> RemovableElement.defaultInit(item, removeAction, titleGenerator) : render;
            return new RemovableListComponent<R>(items,render);
        }
    }
    public static class RemovableElement {

        public static <T> HorizontalLayout defaultInit(T sourceElement, Consumer<T> removeAction, Function<T, String> titleGenerator) {
            return init(sourceElement,
                    removeAction,
                    element -> new ElementListComponent(new ListPrefixComponent(ViewStatic.ELEMENT_PREFIX).initComponent(), titleGenerator.apply(element)));
        }

        public static <T> HorizontalLayout init(T sourceElement, Consumer<T> removeAction, Function<T, Component> componentRender) {
            HorizontalLayout root = new HorizontalLayout();
            Component titleComponent = componentRender.apply(sourceElement);

            root.setClassName("list-element");
            root.setWidthFull();
            root.setDefaultVerticalComponentAlignment(Alignment.CENTER);

            Button deleteButton = new Button(ViewStatic.ELEMENT_DELETE);
            deleteButton.addClassName("transparent-button");

            deleteButton.addClickListener(event -> {
                removeAction.accept(sourceElement);
                root.removeFromParent();
            });

            root.setFlexGrow(1, titleComponent);
            root.add(titleComponent,
                    deleteButton);
            return root;
        }
    }
}

package com.konivan.keepmyart.views.components.icons;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;

public class IconComponent extends Icon {

    private final Icon icon;
    public IconComponent(VaadinIcon vaadinIcon) {
        this.icon = new Icon(vaadinIcon);
    }

    public Component getComponent() {
        return icon;
    }
}

package com.konivan.keepmyart.views.components.list;

import com.konivan.keepmyart.views.ViewStatic;
import com.konivan.keepmyart.views.components.BaseTitleComponent;
import com.konivan.keepmyart.views.components.icons.IconComponent;
import com.vaadin.flow.component.Component;

public class ElementListComponent extends BaseTitleComponent {
    public ElementListComponent(String title) {
        super(ViewStatic.ELEMENT_PREFIX, title);
    }
    IconComponent iconComponent;

    public ElementListComponent(Component prefixComponent, String title) {
        super(prefixComponent, title);
    }

    public ElementListComponent(IconComponent iconComponent, String title) {
        super(iconComponent.getComponent(), title);
        this.iconComponent = iconComponent;
    }

    public IconComponent getIconComponent() {
        return iconComponent;
    }
}

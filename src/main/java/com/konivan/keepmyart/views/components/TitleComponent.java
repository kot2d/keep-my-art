package com.konivan.keepmyart.views.components;

import com.konivan.keepmyart.views.ViewStatic;

public class TitleComponent extends BaseTitleComponent {
    public TitleComponent(String title) {
        super(ViewStatic.ROOT_PREFIX, title);
    }
}

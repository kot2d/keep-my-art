package com.konivan.keepmyart.views.projects;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import lombok.Getter;

import java.util.List;

@Getter
public class ProjectModel {
    private final KeepProject project;
    private final List<KeepSource> list;

    public ProjectModel(KeepProject project, List<KeepSource> list) {
        this.project = project;
        this.list = list;
    }

}

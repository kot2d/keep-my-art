package com.konivan.keepmyart.views.projects;

import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.services.KeepService;
import com.konivan.keepmyart.views.MainLayout;
import com.konivan.keepmyart.views.components.ViewRootComponent;
import com.konivan.keepmyart.views.components.list.ReadOnlyListComponent;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@PageTitle("Projects")
@Route(value = "projects", layout = MainLayout.class)
public class ProjectsView extends ViewRootComponent {
    public ProjectsView(@Autowired KeepService service){
        super("Проекты");
        List<KeepProject> projects = service.getAllProjects();
        List<ProjectModel> projectModels = projects.stream().map(project -> new ProjectModel(project, service.getSourcesByProject(project))).toList();
        ReadOnlyListComponent<ProjectModel> projectList = new ReadOnlyListComponent.Builder<ProjectModel>()
                .setDefaultCssClassName()
                .setItems(projectModels)
                .setRender(ProjectComponent::new)
                .build();
        this.addComponent(projectList);
    }
}

package com.konivan.keepmyart.views.projects;

import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.views.components.SubTitleComponent;
import com.konivan.keepmyart.views.components.list.ReadOnlyListComponent;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.List;

public class ProjectComponent extends VerticalLayout {


    public ProjectComponent(ProjectModel projectModel) {
        this.addClassName("element-project-list");
        SubTitleComponent titleComponent = new SubTitleComponent(projectModel.getProject().getName());
        Span sourcesTitle = new Span("Источники:");

        ReadOnlyListComponent<KeepSource> sourceList = new ReadOnlyListComponent.Builder<KeepSource>()
                .setItems(projectModel.getList())
                .setDefaultCssClassName()
                .setRender(ListElement::new)
                .build();

        Span fileTitle = new Span("Файлы:");
        this.add(titleComponent, sourcesTitle, sourceList, fileTitle);
    }

    class ListElement extends HorizontalLayout {
        public ListElement(KeepSource keepSource){
            this.addClassName("list-element");
            Span sourceType = new Span();
            Span sourceTitle = new Span();
            if(keepSource.getSourceType().equals(SourceTypes.LOCAL)){
                sourceType.setText("LOCAL");
                sourceTitle.setText(keepSource.getSourcePath());
            } else {
                sourceType.setText("YANDEX");
                sourceTitle.setText(keepSource.getUserName());
            }
            this.add(sourceType,sourceTitle);
        }
    }
}

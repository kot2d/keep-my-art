package com.konivan.keepmyart.views.newProject.components;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.localization.TextProvider;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class NewProjectNameLayout extends VerticalLayout {

    public final Binder<KeepProject> projectNameBinder = new Binder<>();

    public final TextField projectName;
    public NewProjectNameLayout(LocalizedText localizedText, KeepProject project) {
        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);
        projectName = new TextField();

        projectNameBinder.forField(projectName)
                .withValidator(
                        name -> name.length() >= 3, textProvider.getErrorText(NewProjectTextProvider.ERROR_PROJECT_NAME_LENGTH))
                .withValidator(
                        name -> name.length() <= 120, textProvider.getErrorText(NewProjectTextProvider.ERROR_PROJECT_MAX_NAME_LENGTH))
                .bind(KeepProject::getName,KeepProject::setName);
        projectNameBinder.setBean(project);

        projectName.setPlaceholder(textProvider.getText(NewProjectTextProvider.PROJECT_NAME));
        projectName.setRequiredIndicatorVisible(true);
        projectName.setClearButtonVisible(true);
        projectName.setWidthFull();
        add(projectName);
    }

    public boolean isOk() {
        return projectNameBinder.validate().isOk();
    }
}

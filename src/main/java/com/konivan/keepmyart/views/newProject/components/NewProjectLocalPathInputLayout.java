package com.konivan.keepmyart.views.newProject.components;

import com.konivan.keepmyart.localization.TextProvider;
import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.function.Consumer;

public class NewProjectLocalPathInputLayout extends TextField {

    private final Binder<KeepSource> localPathBinder;
    private final TextProvider textProvider;

    public NewProjectLocalPathInputLayout(TextProvider textProvider) {
        this.textProvider = textProvider;
        this.setPlaceholder(textProvider.getText(NewProjectTextProvider.ADD_PROJECT_NAME));

        this.localPathBinder = new Binder<>(KeepSource.class);

        this.localPathBinder.forField(this)
                .withValidator(path -> path.length() >= 3,
                        textProvider.getErrorText(NewProjectTextProvider.ERROR_LOCAL_PATH_LENGTH))
                .withValidator(path -> !path.contains("\\"),
                        textProvider.getErrorText(NewProjectTextProvider.ERROR_LOCAL_PATH_DOUBLE_BACK_SLASH))
                .withValidator(path -> !path.contains("//"),
                        textProvider.getErrorText(NewProjectTextProvider.ERROR_LOCAL_PATH_DOUBLE_SLASH))
                .withValidator(path -> !path.contains("."),
                        textProvider.getErrorText(NewProjectTextProvider.ERROR_LOCAL_PATH_DOT))
                .asRequired(textProvider.getErrorText(NewProjectTextProvider.ERROR_LOCAL_PATH_LENGTH))
                .bind(KeepSource::getSourcePath,KeepSource::setSourcePath);
        this.localPathBinder.setBean(new KeepSource());
    }

    public boolean validateInput(Consumer<String> showErrorMessage) {
        if(localPathBinder.validate().isOk()) {
            try {
                Paths.get(getValue());
                return true;
            } catch (InvalidPathException ex) {
                showErrorMessage.accept(textProvider.getErrorText(NewProjectTextProvider.ERROR_PATH_INVALID) + "!\n" + ex.getMessage());
            }
        }
        return false;
    }

    public KeepSource extractLocalSource() {
        KeepSource source = localPathBinder.getBean();
        source.setSourceType(SourceTypes.LOCAL);

        localPathBinder.setBean(new KeepSource());

        return source;
    }

    public Binder<KeepSource> getLocalPathBinder() {
        return localPathBinder;
    }
}

package com.konivan.keepmyart.views.newProject;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.model.*;
import com.konivan.keepmyart.services.KeepService;
import com.konivan.keepmyart.services.configuration.NewProjectSessionService;
import com.konivan.keepmyart.views.MainLayout;
import com.konivan.keepmyart.views.components.ViewRootComponent;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.konivan.keepmyart.views.newProject.components.NewProjectNameLayout;
import com.konivan.keepmyart.views.newProject.components.NewProjectSourceListLayout;
import com.konivan.keepmyart.views.newProject.components.NewProjectSubmitLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@PageTitle("New Project")
@Route(value = WebUrls.NEW_PROJECT, layout = MainLayout.class)
public class NewProjectView extends ViewRootComponent {

    KeepService keepService;
    NewProjectSessionService sessionService;
    NewProjectTextProvider textProvider;

    public NewProjectView(@Autowired NewProjectSessionService sessionService,
                          @Autowired KeepService keepService,
                          @Autowired LocalizedText localizedText) {
        super("Давайте добавим вашь проект!");

        textProvider = new NewProjectTextProvider(localizedText);

        this.keepService = keepService;
        this.sessionService = sessionService;

        KeepProject project = sessionService.getProject();

        NewProjectNameLayout newProjectNameLayout = new NewProjectNameLayout(localizedText, project);

        NewProjectSourceListLayout newProjectSourceListLayout = new NewProjectSourceListLayout(localizedText, project);
        NewProjectSubmitLayout newProjectSubmitLayout = new NewProjectSubmitLayout(
                keepService,
                project,
                localizedText,
                () -> newProjectNameLayout.isOk() && !newProjectSourceListLayout.isEmptyOrSingle(),
                sessionService);

        addComponent(newProjectNameLayout,
                newProjectSourceListLayout,
                newProjectSubmitLayout);
    }
}

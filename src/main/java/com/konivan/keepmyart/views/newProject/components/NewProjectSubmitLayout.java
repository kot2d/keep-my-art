package com.konivan.keepmyart.views.newProject.components;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.services.KeepService;
import com.konivan.keepmyart.services.configuration.NewProjectSessionService;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.konivan.keepmyart.views.projects.ProjectsView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.function.Supplier;

public class NewProjectSubmitLayout extends HorizontalLayout {

    KeepService keepService;

    NewProjectSessionService sessionService;

    public Button submitButton;
    public Button cancelButton;

    public NewProjectSubmitLayout(KeepService keepService,
                                  KeepProject keepProject,
                                  LocalizedText localizedText,
                                  Supplier<Boolean> validator,
                                  NewProjectSessionService sessionService) {
        this.keepService = keepService;
        this.sessionService = sessionService;

        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);

        setWidthFull();
        setJustifyContentMode(JustifyContentMode.CENTER);

        submitButton = new Button(textProvider.getText(NewProjectTextProvider.BUTTON_SAVE));
        submitButton.addClassName("white_button");
        submitButton.addClickListener(event -> {
            if(validator.get()) {
                saveProject(keepProject);
                submitButton.getUI().ifPresent(ui -> ui.navigate(ProjectsView.class));
            }
        });

        cancelButton = new Button(textProvider.getText(NewProjectTextProvider.BUTTON_CANCEL));
        cancelButton.addClassName("white_button");
        cancelButton.addClickListener(event -> {
            cancelProject();
            cancelButton.getUI().ifPresent(ui -> ui.navigate(ProjectsView.class));
        });

        add(submitButton, cancelButton);
    }

    public void saveProject(KeepProject keepProject) {
        keepService.saveProject(keepProject);
        keepService.saveSources(keepProject.getSourceList());
        sessionService.close();
    }

    public void cancelProject() {
        sessionService.close();
    }
}

package com.konivan.keepmyart.views.newProject.components;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.localization.TextProvider;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.views.ViewStatic;
import com.konivan.keepmyart.views.components.ErrorNotificationComponent;
import com.konivan.keepmyart.views.components.SubTitleComponent;
import com.konivan.keepmyart.views.components.icons.CloudIconComponent;
import com.konivan.keepmyart.views.components.icons.LocalSourceIconComponent;
import com.konivan.keepmyart.views.components.list.CloudTitleGenerator;
import com.konivan.keepmyart.views.components.list.ElementListComponent;
import com.konivan.keepmyart.views.components.list.RemovableListComponent;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.konivan.keepmyart.views.oauth2.OauthView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class NewProjectSourceListLayout extends VerticalLayout {

    public final Button addLocalPathButton;
    public final NewProjectLocalPathInputLayout newProjectLocalPathInputLayout;

    private KeepProject project;

    private TextProvider textProvider;

    public NewProjectSourceListLayout(LocalizedText localizedText, KeepProject project) {
        this.project = project;

        this.textProvider = new NewProjectTextProvider(localizedText);

        Button yandexButton = new Button(this.textProvider.getText(NewProjectTextProvider.ADD_YANDEX_CLOUD));
        Button googleButton = new Button(this.textProvider.getText(NewProjectTextProvider.ADD_GOOGLE_CLOUD));
        this.addLocalPathButton = new Button(ViewStatic.ELEMENT_ADD);

        HorizontalLayout localInputLayout = new HorizontalLayout();
        localInputLayout.setWidthFull();

        this.newProjectLocalPathInputLayout = new NewProjectLocalPathInputLayout(this.textProvider);

        RemovableListComponent<KeepSource> localPathList = new RemovableListComponent.Builder<KeepSource>()
                .setItems(this.project.getSourceList())
                .setRender(item -> RemovableListComponent.RemovableElement.init(item,
                        this.project::removeSource, this::setUpTitleElementRender))
                .build();

        addLocalPathButton.addClickListener(event -> {
            if(newProjectLocalPathInputLayout.validateInput(errorMessage -> new ErrorNotificationComponent(errorMessage).open())) {
                KeepSource source = newProjectLocalPathInputLayout.extractLocalSource();
                this.project.addSource(source);
                localPathList.addItem(source);
            }
        });

        yandexButton.addClickListener(e ->
                yandexButton.getUI().ifPresent(ui ->
                        ui.navigate(OauthView.class, SourceTypes.YANDEX.toString()))
        );

        localInputLayout.add(newProjectLocalPathInputLayout, addLocalPathButton, new Span("|"), yandexButton, googleButton);
        localInputLayout.setFlexGrow(1, newProjectLocalPathInputLayout);
        localInputLayout.setDefaultVerticalComponentAlignment(Alignment.CENTER);
        add(new SubTitleComponent(this.textProvider.getText(NewProjectTextProvider.LOCAL_SOURCE_LABEL)), localInputLayout, localPathList);
    }

    public ElementListComponent setUpTitleElementRender(KeepSource keepSource) {
        if(!keepSource.getSourceType().equals(SourceTypes.LOCAL))
            return new ElementListComponent(new CloudIconComponent(), buildTitleTextForSourceListElement(keepSource));
        return new ElementListComponent(new LocalSourceIconComponent(), buildTitleTextForSourceListElement(keepSource));
    }

    private String buildTitleTextForSourceListElement(KeepSource source) {
        switch (source.getSourceType()){
            case YANDEX -> {
                return CloudTitleGenerator.getYandexTitle(source.getUserName());
            }
            case GOOGLE -> {
                return CloudTitleGenerator.getGoogleTitle(source.getUserName());
            }
            default -> {
                return source.getSourcePath();
            }
        }
    }

    public boolean isEmptyOrSingle(){
        if(project.getSourceList().isEmpty()) {
            new ErrorNotificationComponent(textProvider.getErrorText(NewProjectTextProvider.ERROR_EMPTY_SOURCE_LIST)).open();
            return true;
        }
        if(project.getSourceList().size()==1) {
            new ErrorNotificationComponent(textProvider.getErrorText(NewProjectTextProvider.ERROR_SINGLE_SOURCE_IN_LIST)).open();
            return true;
        }
        return false;
    }
}

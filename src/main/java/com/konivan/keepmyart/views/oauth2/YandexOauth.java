package com.konivan.keepmyart.views.oauth2;

import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.model.WebUrls;
import com.konivan.keepmyart.services.clouds.YandexService;
import com.konivan.keepmyart.services.configuration.NewProjectSessionService;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@PageTitle("OauthProcess")
@Route(value = WebUrls.OAUTH2_YANDEX)
public class YandexOauth extends Div implements HasUrlParameter<String> {

    NewProjectSessionService sessionService;

    public YandexOauth(@Autowired NewProjectSessionService sessionService){
        this.sessionService = sessionService;
    }

    @Override
    public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters
                .getParameters();

        String oauthCode = parametersMap.get("code").get(0);

        YandexService yaService  = new YandexService();

        try {
            yaService.doOauth(oauthCode);
            KeepSource source = new KeepSource();
            source.setCloudToken(yaService.getOauthToken());
            source.setSourceType(SourceTypes.YANDEX);
            source.setUserName(yaService.getUserInfo(yaService.getOauthToken()).getLogin());
            source.setSourcePath("keep_my_art");
            sessionService.getProject().addSource(source);
        } catch (IOException | ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }

        event.forwardToUrl(WebUrls.NEW_PROJECT);
    }
}

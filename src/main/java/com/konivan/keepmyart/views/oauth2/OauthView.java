package com.konivan.keepmyart.views.oauth2;

import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.model.WebUrls;
import com.konivan.keepmyart.services.clouds.YandexService;
import com.konivan.keepmyart.views.components.SubTitleComponent;
import com.konivan.keepmyart.views.components.TitleComponent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@PageTitle("OauthWarning")
@Route(value = WebUrls.OAUTH2)
public class OauthView extends VerticalLayout implements HasUrlParameter<String> {

    String targetCloud;
    Button startButton;
    private final String title = "Внимание";
    private final String text = "Приложение запросит разрешение на использование вашего облака " +
            "дляхранения и обновления файлов Вашего проекта. Облачный сервис выделить для Вашего проекта отдельную" +
            "директорию, приложение будет иметь доступ только для этой директории.";

    public OauthView(){
        YandexService yaService =  new YandexService();
        this.setAlignItems(FlexComponent.Alignment.CENTER);
        VerticalLayout innerLayout = new VerticalLayout();
        innerLayout.setWidth("50%");
        startButton = new Button();

        startButton.addClickListener(event -> {
            if(SourceTypes.valueOf(targetCloud).equals(SourceTypes.YANDEX)){
                UI.getCurrent().getPage().setLocation(yaService.getAuthorizationUrl());
            }
        });
        innerLayout.add(new TitleComponent(title),
                new SubTitleComponent(text),
                startButton);
        add(innerLayout);
    }

    @Override
    public void setParameter(BeforeEvent event, String parameter) {
        startButton.setText(parameter);
        targetCloud = parameter;
    }
}

package com.konivan.keepmyart.views.localization;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.localization.TextProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommonTextProvider extends TextProvider {

    private final LocalizedText localizedText;

    public final static TextSource BUTTON_SAVE = new TextSource("text/common", "button_save");
    public final static TextSource BUTTON_CANCEL = new TextSource("text/common", "button_cancel");
    public final static TextSource BUTTON_CREATE_PROJECT = new TextSource("text/common", "button_create_project");
    @Autowired
    public CommonTextProvider(LocalizedText localizedText) {
        this.localizedText = localizedText;
    }

    @Override
    public String getErrorText(TextSource textSource) {
        return localizedText.getText(textSource.getSource(), textSource.getText());
    }

    @Override
    public String getText(TextSource textSource) {
        return localizedText.getText(textSource.getSource(), textSource.getText());
    }
}

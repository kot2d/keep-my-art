package com.konivan.keepmyart.views.localization;

import com.konivan.keepmyart.localization.LocalizedText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NewProjectTextProvider extends CommonTextProvider {

    static final String MESSAGE_PATH = "text/new_project_page";
    static final String ERROR_PATH = "text/errors";
    @Autowired
    public NewProjectTextProvider(LocalizedText localizedText) {
        super(localizedText);
    }

    public final static TextSource ERROR_LOCAL_PATH_LENGTH = new TextSource(ERROR_PATH, "new_project.error_path_length");
    public final static TextSource ERROR_LOCAL_PATH_DOUBLE_SLASH = new TextSource(ERROR_PATH, "new_project.error_path_double_slash");
    public final static TextSource ERROR_LOCAL_PATH_DOUBLE_BACK_SLASH = new TextSource(ERROR_PATH, "new_project.error_path_double_back_slash");
    public final static TextSource ERROR_LOCAL_PATH_DOT = new TextSource(ERROR_PATH, "new_project.error_path_dot");
    public final static TextSource ERROR_PROJECT_NAME_LENGTH = new TextSource(ERROR_PATH, "new_project.error_project_name_length");
    public final static TextSource ERROR_PROJECT_MAX_NAME_LENGTH = new TextSource(ERROR_PATH, "new_project.error_project_name_length");
    public final static TextSource ERROR_PATH_INVALID = new TextSource(ERROR_PATH, "new_project.error_path_invalid");
    public final static TextSource ERROR_EMPTY_SOURCE_LIST = new TextSource(ERROR_PATH, "new_project.error_source_list_is_empty");
    public final static TextSource ERROR_SINGLE_SOURCE_IN_LIST = new TextSource(ERROR_PATH, "new_project.error_only_single_source");
    public final static TextSource ADD_PROJECT_NAME = new TextSource(MESSAGE_PATH, "add_project_name");
    public final static TextSource PROJECT_NAME = new TextSource(MESSAGE_PATH, "project_name");
    public final static TextSource ADD_YANDEX_CLOUD = new TextSource(MESSAGE_PATH, "add_yandex_cloud");
    public final static TextSource ADD_GOOGLE_CLOUD = new TextSource(MESSAGE_PATH, "add_google_cloud");
    public final static TextSource LOCAL_SOURCE_LABEL = new TextSource(MESSAGE_PATH, "local_source_label");


}

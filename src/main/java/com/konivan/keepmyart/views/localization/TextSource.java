package com.konivan.keepmyart.views.localization;

public class TextSource {
    private final String source;
    private final String text;

    public TextSource(String source, String text) {
        this.source = source;
        this.text = text;
    }

    public String getSource() {
        return source;
    }

    public String getText() {
        return text;
    }
}

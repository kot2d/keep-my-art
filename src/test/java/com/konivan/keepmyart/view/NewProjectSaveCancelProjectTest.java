package com.konivan.keepmyart.view;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.services.KeepService;
import com.konivan.keepmyart.services.configuration.NewProjectSessionService;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.konivan.keepmyart.views.newProject.components.NewProjectSubmitLayout;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@SpringBootTest
public class NewProjectSaveCancelProjectTest {

    @Mock
    KeepService keepService;

    @Autowired
    LocalizedText localizedText;

    @ParameterizedTest
    @MethodSource("getGoodProjects")
    public void testSaveProject(KeepProject keepProject) {
        when(keepService.saveProject(keepProject)).thenReturn(keepProject);
        NewProjectSessionService sessionService = new NewProjectSessionService();
        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);
        sessionService.setProject(keepProject);
        NewProjectSubmitLayout submitLayout = new NewProjectSubmitLayout(keepService, keepProject, localizedText, () -> true, sessionService);
        submitLayout.submitButton.click();
        Mockito.verify(keepService, times(1)).saveProject(keepProject);
        assert !sessionService.getProject().equals(keepProject);
    }

    @Test
    public void testNegativeSaveProject() {
        KeepSource localKeepSource = new KeepSource();
        localKeepSource.setSourceType(SourceTypes.LOCAL);
        localKeepSource.setSourcePath("/test/test");

        KeepProject keepProject = new KeepProject();
        keepProject.addSource(localKeepSource);

        when(keepService.saveProject(keepProject)).thenReturn(keepProject);
        NewProjectSessionService sessionService = new NewProjectSessionService();
        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);
        sessionService.setProject(keepProject);
        NewProjectSubmitLayout submitLayout = new NewProjectSubmitLayout(keepService, keepProject, localizedText, () -> false, sessionService);
        submitLayout.cancelButton.click();
        Mockito.verify(keepService, times(0)).saveProject(keepProject);
        assert !sessionService.getProject().equals(keepProject);
    }

    @Test
    public void testCancelProject() {
        KeepSource localKeepSource = new KeepSource();
        localKeepSource.setSourceType(SourceTypes.LOCAL);
        localKeepSource.setSourcePath("/test/test");

        KeepProject keepProject = new KeepProject();
        keepProject.addSource(localKeepSource);

        when(keepService.saveProject(keepProject)).thenReturn(keepProject);
        NewProjectSessionService sessionService = new NewProjectSessionService();
        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);
        sessionService.setProject(keepProject);
        NewProjectSubmitLayout submitLayout = new NewProjectSubmitLayout(keepService, keepProject, localizedText, () -> true, sessionService);
        submitLayout.cancelButton.click();
        Mockito.verify(keepService, times(0)).saveProject(keepProject);
        assert !sessionService.getProject().equals(keepProject);
    }

    static Stream<KeepProject> getGoodProjects() {
        KeepSource localKeepSource1 = new KeepSource();
        localKeepSource1.setSourceType(SourceTypes.LOCAL);
        localKeepSource1.setSourcePath("/test/test");

        KeepSource localKeepSource2 = new KeepSource();
        localKeepSource2.setSourceType(SourceTypes.LOCAL);
        localKeepSource2.setSourcePath("/test/test");

        KeepSource cloudKeepSource1 = new KeepSource();
        cloudKeepSource1.setSourceType(SourceTypes.YANDEX);
        cloudKeepSource1.setSourcePath("/test/test");
        cloudKeepSource1.setUserName("test_username");

        KeepSource cloudKeepSource2 = new KeepSource();
        cloudKeepSource2.setSourceType(SourceTypes.GOOGLE);
        cloudKeepSource2.setSourcePath("/test/test");
        cloudKeepSource2.setUserName("test_username");

        KeepProject keepProject1 = new KeepProject();
        keepProject1.addSource(localKeepSource1);
        keepProject1.addSource(localKeepSource2);

        KeepProject keepProject2 = new KeepProject();
        keepProject2.addSource(localKeepSource1);
        keepProject2.addSource(cloudKeepSource1);

        KeepProject keepProject3 = new KeepProject();
        keepProject3.addSource(cloudKeepSource1);
        keepProject3.addSource(cloudKeepSource2);

        return Stream.of(keepProject1, keepProject2, keepProject3);
    }

}

package com.konivan.keepmyart.view;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.views.components.icons.CloudIconComponent;
import com.konivan.keepmyart.views.components.icons.LocalSourceIconComponent;
import com.konivan.keepmyart.views.components.list.ElementListComponent;
import com.konivan.keepmyart.views.newProject.components.NewProjectSourceListLayout;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class NewProjectSectionsTest {
    static KeepSource localShort = new KeepSource();
    static KeepSource localLong = new KeepSource();
    static KeepSource yandexCloud = new KeepSource();
    NewProjectTextProvider textProvider;
    LocalizedText localizedText;
    @BeforeAll
    @Autowired
    public void init(LocalizedText localizedText) {
        this.localizedText = localizedText;
        this.textProvider = new NewProjectTextProvider(localizedText);
        localShort.setSourcePath("C:\\");
        localShort.setSourceType(SourceTypes.LOCAL);

        localLong.setSourcePath("C:\\test\\test\\test\\test\\test\\test\\test\\test\\test\\test\\test\\test\\test\\test\\test");
        localLong.setSourceType(SourceTypes.LOCAL);

        yandexCloud.setUserName("Yandex_test_user");
        yandexCloud.setSourceType(SourceTypes.YANDEX);
    }
    @Test
    public void testLocalSourceIconSourceList() {
        NewProjectSourceListLayout sourceListSection = new NewProjectSourceListLayout(this.textProvider, new KeepProject());
        ElementListComponent testElement = sourceListSection.setUpTitleElementRender(localLong);
        assert testElement.getIconComponent() instanceof LocalSourceIconComponent;
    }

    @Test
    public void testCloudSourceIconSourceList() {
        NewProjectSourceListLayout sourceListSection = new NewProjectSourceListLayout(this.textProvider, new KeepProject());
        ElementListComponent testElement = sourceListSection.setUpTitleElementRender(yandexCloud);
        assert testElement.getIconComponent() instanceof CloudIconComponent;
    }
}

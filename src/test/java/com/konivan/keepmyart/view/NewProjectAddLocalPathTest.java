package com.konivan.keepmyart.view;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.localization.TextProvider;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.model.KeepSource;
import com.konivan.keepmyart.model.SourceTypes;
import com.konivan.keepmyart.views.localization.TextSource;
import com.konivan.keepmyart.views.newProject.components.NewProjectSourceListLayout;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.vaadin.flow.data.binder.ValidationResult;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;

import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@SpringBootTest
public class NewProjectAddLocalPathTest {

    static KeepSource localNormal = new KeepSource();
    static KeepSource localShort = new KeepSource();
    static KeepSource localLong = new KeepSource();


    @Autowired
    LocalizedText localizedText;

    static NewProjectTextProvider textProvider;


    @BeforeAll
    public static void init() {
        localShort.setSourcePath("c:/");
        localShort.setSourceType(SourceTypes.LOCAL);

        localNormal.setSourcePath("c:/test/test");
        localNormal.setSourceType(SourceTypes.LOCAL);

        localLong.setSourcePath("c:/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test/test");
        localLong.setSourceType(SourceTypes.LOCAL);
    }

    @ParameterizedTest
    @MethodSource("generateLocalPath")
    public void testAddLocalPathButtonAndProjectSourceList(String localPathValue, KeepSource keepSource) {
        textProvider = new NewProjectTextProvider(localizedText);
        KeepProject testKeepProject = new KeepProject();
        NewProjectSourceListLayout sourceListSection = new NewProjectSourceListLayout(localizedText, testKeepProject);
        sourceListSection.newProjectLocalPathInputLayout.setValue(localPathValue);
        sourceListSection.addLocalPathButton.click();
        assertAll(
                () -> assertEquals(testKeepProject.getSourceList().size(), 1, "Source List length"),
                () -> assertEquals(testKeepProject.getSourceList().get(0).getSourceType(), keepSource.getSourceType(), "Source Type Assertion"),
                () -> assertEquals(testKeepProject.getSourceList().get(0).getSourcePath(), keepSource.getSourcePath(), "Source Local Path Assertion"),
                () -> assertEquals(testKeepProject.getSourceList().get(0).getUserName(), keepSource.getUserName(), "Source User Name Assertion"),
                () -> assertEquals(testKeepProject.getSourceList().get(0).getCloudToken(), keepSource.getCloudToken(), "Source Cloud Token Assertion"),
                () -> assertNotNull(testKeepProject.getSourceList().get(0).getId(), "Source Id Not Null Assertion"),
                () -> assertEquals(testKeepProject.getSourceList().get(0).getKeepProject().getId(), testKeepProject.getId(), "Source Parent Project Id Assertion")
                );
    }

    @ParameterizedTest
    @MethodSource("generateWrongLocalPath")
    public void testAddLocalPathButtonWithInvalidPath(String localPathValue, TextSource error) {
        TextProvider textProvider = new NewProjectTextProvider(localizedText);

        NewProjectSourceListLayout sourceListSection = new NewProjectSourceListLayout(localizedText, new KeepProject());

        sourceListSection.newProjectLocalPathInputLayout.setValue(localPathValue);
        List<ValidationResult> errors = sourceListSection.newProjectLocalPathInputLayout.getLocalPathBinder().validate().getValidationErrors();
        assertEquals(textProvider.getErrorText(error), errors.get(0).getErrorMessage());
    }

    @ParameterizedTest
    @MethodSource("generateWrongComplexLocalPath")
    public void testAddLocalPathButtonWithComplexInvalidPath(String localPathValue) {
        TextProvider textProvider = new NewProjectTextProvider(localizedText);

        NewProjectSourceListLayout sourceListSection = new NewProjectSourceListLayout(localizedText, new KeepProject());

        sourceListSection.newProjectLocalPathInputLayout.setValue(localPathValue);
        assertFalse(sourceListSection.newProjectLocalPathInputLayout.validateInput(error -> {}));
    }

    private static Stream<Arguments> generateLocalPath() {
        return Stream.of(arguments(localShort.getSourcePath(), localShort)
        , arguments(localNormal.getSourcePath(), localNormal)
        , arguments(localLong.getSourcePath(), localLong));
    }

    private static Stream<Arguments> generateWrongLocalPath() {
        return Stream.of(
                arguments("C:", NewProjectTextProvider.ERROR_LOCAL_PATH_LENGTH),
                arguments("", NewProjectTextProvider.ERROR_LOCAL_PATH_LENGTH),
                arguments("C", NewProjectTextProvider.ERROR_LOCAL_PATH_LENGTH),
                arguments("C://", NewProjectTextProvider.ERROR_LOCAL_PATH_DOUBLE_SLASH),
                arguments("C:test/..", NewProjectTextProvider.ERROR_LOCAL_PATH_DOT),
                arguments("C:test/.", NewProjectTextProvider.ERROR_LOCAL_PATH_DOT),
                arguments("C:test/./", NewProjectTextProvider.ERROR_LOCAL_PATH_DOT),
                arguments("C:/test//", NewProjectTextProvider.ERROR_LOCAL_PATH_DOUBLE_SLASH),
                arguments("C:/test\\", NewProjectTextProvider.ERROR_LOCAL_PATH_DOUBLE_BACK_SLASH)
        );
    }

    private static Stream<String> generateWrongComplexLocalPath() {
        return Stream.of(" :/",
                "ZZZZ:/",
                "C:/?"
        );
    }



}

package com.konivan.keepmyart.view;

import com.konivan.keepmyart.localization.LocalizedText;
import com.konivan.keepmyart.model.KeepProject;
import com.konivan.keepmyart.views.localization.NewProjectTextProvider;
import com.konivan.keepmyart.views.localization.TextSource;
import com.konivan.keepmyart.views.newProject.components.NewProjectNameLayout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

@SpringBootTest
public class NewProjectNameTest {

    @Autowired
    LocalizedText localizedText;

    @ParameterizedTest
    @MethodSource("getValidNames")
    public void testPositiveValidateName(String name) {
        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);
        KeepProject keepProject = new KeepProject();
        NewProjectNameLayout section = new NewProjectNameLayout(localizedText, keepProject);
        section.projectName.setValue(name);
        assert section.isOk();
    }

    @ParameterizedTest
    @MethodSource("getInvalidNames")
    public void testNegateValidateName(String name, TextSource errorPath) {
        NewProjectTextProvider textProvider = new NewProjectTextProvider(localizedText);
        KeepProject keepProject = new KeepProject();
        NewProjectNameLayout section = new NewProjectNameLayout(localizedText, keepProject);
        section.projectName.setValue(name);
        assert !section.isOk();
        assert section.projectNameBinder.validate().getValidationErrors().get(0).getErrorMessage().equals(textProvider.getErrorText(errorPath));
    }

    private static Stream<String> getValidNames() {
        return Stream.of("Tes",
                "Test",
                "Test Test",
                "Test !@#$%^&*()_+|=`",
                "Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test "
        );
    }

    private static Stream<Arguments> getInvalidNames() {
        return Stream.of(Arguments.arguments("Te", NewProjectTextProvider.ERROR_PROJECT_NAME_LENGTH),
                Arguments.arguments("Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test T", NewProjectTextProvider.ERROR_PROJECT_MAX_NAME_LENGTH)
        );
    }
}
